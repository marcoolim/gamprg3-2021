using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    //Camera Code reference: https://www.youtube.com/watch?v=Nc7h5-pLFqg

    public float scrollSpeed;
    public float topLimit;
    public float botLimit;
    public float leftLimit;
    public float rightLimit;

    void Update()
    {
        if (Input.mousePosition.y >= Screen.height * topLimit)
        {
            transform.Translate(Vector3.forward * Time.deltaTime * scrollSpeed, Space.World);
        }

        if (Input.mousePosition.y <= Screen.height * botLimit)
        {
            transform.Translate(Vector3.back * Time.deltaTime * scrollSpeed, Space.World);
        }

        if (Input.mousePosition.x >= Screen.width * rightLimit)
        {
            transform.Translate(Vector3.right * Time.deltaTime * scrollSpeed, Space.World);
        }

        if (Input.mousePosition.x <= Screen.width * leftLimit)
        {
            transform.Translate(Vector3.left * Time.deltaTime * scrollSpeed, Space.World);
        }

        //Clamp X
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, -240f, -60f), transform.position.y, transform.position.z);

        //Clamp Z
        transform.position = new Vector3(transform.position.x, transform.position.y, Mathf.Clamp(transform.position.z, -305f, -100f));

    }
}
