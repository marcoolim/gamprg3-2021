using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class TimeController : MonoBehaviour
{
    public Light mapLight;
    public Text timeText;
    public float seconds;
    public float minutes;
    private bool dayLight = true;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("LightCycle", 300, 300); // day and night every 5 mins. 300 secs = 5 mins
    }

    // Update is called once per frame
    void Update()
    {
        seconds += Time.deltaTime;

        if (seconds >= 60f)
        {
            minutes++;
            seconds = 0; ;
        }
        timeText.text = minutes + ":" + seconds.ToString("00");
    }

    public void LightCycle()
    {
        dayLight = !dayLight;

        if (dayLight)
        {
            mapLight.color = Color.white;

        }

        else
        {
            mapLight.color = Color.black;

        }

    }
}
